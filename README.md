# One Small Dome

Agnes Camera, Sara Falcone and I made this tiny dome for 99 Fridays in February 2018.

## Drawing the Dome

OK, using this Rhino Polyhedra Plugin I found ages ago and Grasshopper, I make a tiny sketch to count the edges and look at sizes.

![one](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/one.png)

We ended up opting for a much simpler design. This way we only have to make one type of joint, and cut one length of member.

![two](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/two.png)

Then I sketched a little sheet metal joint:

![three](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/three.png)

And unrolled and refined the pattern, adding some notches for folding reference.

![four](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/four.png)

## Fab

We cut these on the Fab Light:

![fablight](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/fablight.jpg)

And folded them on the hand brake:

![folding](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/folding.jpg)

![folded](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/folded.jpg)

We welded the tabs together:

![welding](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/welding-one.jpg)

![welding](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/welding-two.jpg)

![welding](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/welding-three.jpg)

## Assembly

Putting it together took ~45 minutes

![assembly](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/assembly-one.jpg)

![assembly](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/assembly-two.jpg)

![assembly](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/assembly-three.jpg)

## Together

This was fun! We left some yarn about to weave into the panels. Participatory architecture!

![weaving](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/weaving-one.jpg)

![weaving](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/weaving-two.jpg)

![weaving](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/fin.jpg)

## RIP Dodecadome

2018 Feb 9 - 2018 Feb 10

![rip](https://gitlab.cba.mit.edu/jakeread/dodecadome/raw/master/images/rip.jpg)